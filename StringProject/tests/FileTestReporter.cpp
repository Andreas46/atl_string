#include <unittest++/UnitTest++.h>
#include <unittest++/TestDetails.h>

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string.h>

#include "FileTestReporter.h"

FileTestReporter::FileTestReporter (std::ostream& targetOutput)
{
	hasTestFailed = false;

	output = &targetOutput;
}

FileTestReporter::~FileTestReporter (void)
{
}

int FileTestReporter::runAllUnitTests (void)
{
	(*output) << "--- (" << getTimeStamp() << ") ---" << std::endl;
	(*output) << "--- Beginning all unit tests ---" << std::endl;

	UnitTest::TestRunner runner (*this);

	//retCode = UnitTest::RunAllTests();
	return runner.RunTestsIf (UnitTest::Test::GetTestList(), NULL, UnitTest::True(), 0);
}

void FileTestReporter::ReportTestStart (UnitTest::TestDetails const& test)
{
	(*output) << test.testName << "	";
}

void FileTestReporter::ReportFailure (UnitTest::TestDetails const& test, char const* failure)
{
	(*output) << "[FAILED]" << std::endl;
	(*output) << "Failed in " << test.filename << ": " << test.lineNumber << std::endl;
	(*output) << "Check that failed: " << failure << std::endl;

	hasTestFailed = true;
}

void FileTestReporter::ReportTestFinish (UnitTest::TestDetails const& test, float secondsElapsed)
{
	if (hasTestFailed == false)
	{
		(*output) << "[PASS]" << std::endl;
	}

	hasTestFailed = false;
}

void FileTestReporter::ReportSummary (int totalTestCount, int failedTestCount, int failureCount, float secondsElapsed)
{
	(*output) << std::endl;

	if (failureCount > 0)
	{
        //std::printf("FAILURE: %d out of %d tests failed (%d failures).\n", failedTestCount, totalTestCount, failureCount);
		(*output) << "FAILURE: " << failedTestCount << " out of " << totalTestCount << " tests failed (";
		(*output) << failureCount << " failures)." << std::endl;
	}
    else
	{
        //std::printf("Success: %d tests passed.\n", totalTestCount);
		(*output) << "Success: " << totalTestCount << " tests passed." << std::endl;
	}

	//printf("Test time: %.2f seconds.\n", secondsElapsed);
	(*output) << "Test time: " << secondsElapsed << " seconds." << std::endl;
}

std::string FileTestReporter::getTimeStamp (void)
{
	char* currTime;
	time_t timeStruct;

	timeStruct = time(NULL);

	currTime = ctime (&timeStruct);

	// -1 to crop off last "/n" returned from "ctime" function.
	return std::string (currTime, strlen (currTime) - 1);
}

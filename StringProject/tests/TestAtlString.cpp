#include <unittest++/UnitTest++.h>

#include "AtlString.h"

#include <stdexcept>
#include <iostream>
#include <string.h>

class StringMockObject: public ATL::String
{
public:
	StringMockObject (void) {}

	StringMockObject (const String& rhs): String (rhs) {}

	StringMockObject (const char* cStr): String (cStr) {}

	StringMockObject (const char* byteArry, const uint32_t numBytes): String (byteArry, numBytes) {}

	StringMockObject (const double inInt): String (inInt) {}

	char* exposeCarray (void)
	{
		return data;
	}

	void setInStream (std::istream* newInStream)
	{
		inStream = newInStream;
	}

	 void setOutStream (std::ostream* newOutStream)
	{
		outStream = newOutStream;
	}
};

SUITE (TestAslString)
{
	// These two tests are just to catch initialisation/destruction exceptions.	
	TEST (TestDefaultConstructor)
	{
		ATL::String string;
	}
	TEST (TestDefaultDestructor)
	{
		ATL::String string;
	}

	TEST (TestCStrConstructor)
	{
		const char testStr[] = "testString";

		StringMockObject str (testStr);

		// Test for data equality.
		CHECK (strncmp (testStr, str.exposeCarray(), strlen (testStr)) == 0);

		// Test data length.
		CHECK (strlen (testStr) == str.length());
	}

	TEST (TestDefaultInitialiserConstructor)
	{
		const char testStr[] = "testString2";

		StringMockObject str (testStr);

		StringMockObject str2 (str);

		// Test for data equality.
		CHECK (strncmp (str2.exposeCarray(), str.exposeCarray(), strlen (testStr)) == 0);

		// Test data length.
		CHECK (strlen (testStr) == str.length());
	}

	TEST (TestCharArrayConstructor)
	{
		const char testCStr[] = "Some MOre tesed!@!";

		StringMockObject testStr (testCStr, strlen (testCStr));

		// Test for data equality.
		CHECK (strncmp (testStr.exposeCarray(),testCStr, strlen (testCStr)) == 0);

		// Test data length.
		CHECK (strlen (testCStr) == testStr.length());
	}

	TEST (TestIntegerConstructor)
	{
		const double testInt = -983.01;
		const char testIntStr[] = "-983.01"; // c string representation of integer.

		StringMockObject testStr (testInt);

		// Test for data equality.
		CHECK (strncmp (testStr.exposeCarray(), testIntStr, strlen (testIntStr)) == 0);

		// Test data length.
		CHECK (strlen (testIntStr) == testStr.length());
	}

	TEST (TestCharConstructor)
	{
		const char testChar = 'C';

		//StringMockObject testStr (testCStr, strlen (testCStr));
		ATL::String testStr (testChar);

		// Test for data equality.
		CHECK (testStr == "C");

		// Test data length.
		CHECK (testStr.length() == 1);
	}
	
	TEST (TestBoolConstructor)
	{
		bool testBoolFalse = false;
		bool testBoolTrue = true;
		
		ATL::String aString (testBoolFalse);
		ATL::String aString2 (testBoolTrue);
		
		CHECK (aString == "false");
		CHECK (aString2 == "true");
	}

	TEST (TestCharAt)
	{
		StringMockObject str ("01234567");

		CHECK (str.charAt (0) == '0');
		CHECK (str.charAt (4) == '4');
		CHECK (str.charAt (7) == '7'); // Test last array item

		// Check that an exception is thrown for out-of-bounds indexes.
		CHECK_THROW (str.charAt (8), std::runtime_error);
	}

	
	TEST (TestCharAtReference)
	{
		StringMockObject str ("01234567");
		StringMockObject expectedResult ("0123!567");

		CHECK (str.charAt (4) == '4');

		str.charAt (4) = '!';

		CHECK (str == expectedResult);
	}

	TEST (TestEqualToFunc)
	{
		ATL::String testString1 ("test1");
		ATL::String testString2 ("test1");
		ATL::String testString3 ("Not test1");
		ATL::String testString4 ("test4");

		CHECK (testString1.equalTo (testString2) == true);
		CHECK (testString1.equalTo (testString1) == true);  // What happens when compared to self?
		CHECK (testString1.equalTo (testString3) == false);
		CHECK (testString1.equalTo (testString4) == false); // Different content, same length.
	}
	
	TEST (TestEmpty)
	{
		ATL::String testString1 ("test1");
		ATL::String testString2 ("");
		ATL::String testString3;
		
		CHECK (testString1.empty() == false);
		CHECK (testString2.empty() == true);
		CHECK (testString3.empty() == true);
		
		testString1.clear();
		CHECK (testString1.empty() == true);
	}
	
	TEST (TestClear)
	{
		ATL::String testString1 ("test1");
		ATL::String testString2 ("");
		ATL::String testString3;
		
		testString1.clear();
		testString2.clear();
		testString3.clear();
		
		CHECK (testString1.empty() == true);
		CHECK (testString2.empty() == true);
		CHECK (testString3.empty() == true);
	}

	TEST (TestAppendCharArray)
	{
		ATL::String testString1 ("test1");
		const char appStr[] = "hello";

		ATL::String* RetPtr;

		RetPtr = &(testString1.append (appStr, strlen (appStr)));

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1hello"));
	}

	TEST (TestAppendString)
	{
		ATL::String testString1 ("test1");
		ATL::String testString2 ("world");

		ATL::String* RetPtr;

		RetPtr = &(testString1.append (testString2));

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1world"));
	}

	TEST (TestAppendChar)
	{
		ATL::String testString1 ("test1");
		const char testChar = 'C';

		ATL::String* RetPtr;

		RetPtr = &(testString1.append (testChar));

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1C"));
	}

	TEST (TestAppendDouble)
	{
		ATL::String testString1 ("test1");
		ATL::String expectedResult ("test1617.37");
		const double testInt = 617.37;

		ATL::String* RetPtr;

		RetPtr = &(testString1.append (testInt));

		CHECK (RetPtr == &testString1);
		CHECK (testString1.equalTo (expectedResult));
	}

	TEST (TestPrependString)
	{
		ATL::String testString1 ("Test1");
		ATL::String testString2 ("word");

		ATL::String* RetPtr;

		RetPtr = &(testString1.prepend (testString2));

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("wordTest1"));
	}

	TEST (TestToUpper)
	{
		ATL::String testString ("a tEst");
		ATL::String expectedResult ("A TEST");
		ATL::String emptyStr;

		// Make sure there is no crash or anything on an empty string.
		emptyStr.toUpper();

		testString.toUpper();

		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestToLower)
	{
		ATL::String testString ("AnOTHer tEst");
		ATL::String expectedResult ("another test");
		ATL::String emptyStr;

		// Make sure there is no crash or anything on an empty string.
		emptyStr.toLower();

		testString.toLower();

		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestFind)
	{
		ATL::String testString ("Here, have some gum");
		ATL::String notInTestString ("another test");
		ATL::String bigString ("hsdlkfjeialksdj asdklfjaoiweu34809ad 0sadfksadljf8234j");
		ATL::String inTestString ("have");
		ATL::String endOfString ("gum");
		ATL::String emptyStr;

		// Should never find anything in an empty test string.
		CHECK (emptyStr.find (emptyStr) == -1);
		CHECK (emptyStr.find (inTestString) == -1);

		CHECK (testString.find (notInTestString) == -1);
		CHECK (testString.find (emptyStr) == -1);
		CHECK (testString.find (bigString) == -1);

		CHECK (testString.find (inTestString) == 6);
		CHECK (testString.find (endOfString) == 16);

		// Test found with startPos
		CHECK (testString.find (inTestString, 6) == 6);
		CHECK (testString.find (inTestString, 7) == -1);
	}

	TEST (TestReplaceNotFound)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String replaceStr ("ARG!");

		ATL::String bigString ("hsdlkfjeialksdj asdklfjaoiweu34809ad 0sadfksadljf8234j");
		ATL::String emptyStr;
		ATL::String noFind ("DLKJFE");

		uint64_t numReplacments = 0;

		// Sanity check.
		CHECK (testString.equalTo (origTestString));

		numReplacments = testString.replace (bigString, replaceStr);
		CHECK (numReplacments == 0);
		CHECK (testString.equalTo (origTestString));

		numReplacments = testString.replace (emptyStr, replaceStr);
		CHECK (numReplacments == 0);
		CHECK (testString.equalTo (origTestString));

		numReplacments = testString.replace (noFind, replaceStr);
		CHECK (numReplacments == 0);
		CHECK (testString.equalTo (origTestString));
	}

	TEST (TestReplaceMany)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 ARG! 456 ARG! 789ARG!ARG!");
		ATL::String testString (origTestString);
		ATL::String multiFind ("BANG!");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments = 0;

		numReplacments = testString.replace (multiFind, replaceStr);
		CHECK (numReplacments == 4);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestReplaceManyWholeWord)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 ARG! 456 ARG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String multiFind ("BANG!");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments = 0;

		numReplacments = testString.replace (multiFind, replaceStr, true, 0, true);
		CHECK (numReplacments == 2);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestReplaceFirstReplace)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 ARG! 456 BANG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String multiFind ("BANG!");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments;

		numReplacments = testString.replace (multiFind, replaceStr, false);
		CHECK (numReplacments == 1);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestReplaceOneOfStart)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("ARG! BANG! 456 BANG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String oneFindStart ("123");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments;

		numReplacments = testString.replace (oneFindStart, replaceStr);
		CHECK (numReplacments == 1);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestReplaceOneOfMiddle)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 BANG! ARG! BANG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String oneFind ("456");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments;

		numReplacments = testString.replace (oneFind, replaceStr);
		CHECK (numReplacments == 1);
		CHECK (testString.equalTo (expectedResult));
	}
	
	TEST (TestReplaceManyStartPos)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 BANG! 456 ARG! 789ARG!ARG!");
		ATL::String testString (origTestString);
		ATL::String multiFind ("BANG!");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments = 0;

		numReplacments = testString.replace (multiFind, replaceStr, true, 14);
		CHECK (numReplacments == 3);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestReplaceOneStartPos)
	{
		ATL::String origTestString ("123 BANG! 456 BANG! 789BANG!BANG!");
		ATL::String expectedResult ("123 BANG! 456 ARG! 789BANG!BANG!");
		ATL::String testString (origTestString);
		ATL::String multiFind ("BANG!");
		ATL::String replaceStr ("ARG!");

		uint64_t numReplacments = 0;

		numReplacments = testString.replace (multiFind, replaceStr, false, 14);
		CHECK (numReplacments == 1);
		CHECK (testString.equalTo (expectedResult));
	}

	TEST (TestGetNextWord)
	{
		ATL::String testStr1 ("here, have some gum!");

		CHECK (testStr1.getNextWord () == "here");
		CHECK (testStr1.getNextWord (6) == "have");
		// Test 1 character past the last, returns empty string.
		CHECK (testStr1.getNextWord (testStr1.length()) == "");
		CHECK (testStr1.getNextWord (17) == "um");
		// Test to make sure leading white space is skipped.
		CHECK (testStr1.getNextWord (5) == "have");
	}

	TEST (TestFindWordPosInString)
	{
		ATL::String testStr1 ("here, have some gum!");

		CHECK (testStr1.findWordPosInString ("have") == 6);
		CHECK (testStr1.findWordPosInString ("have", 8) == -1);
		CHECK (testStr1.findWordPosInString ("hav") == -1);
	}

	TEST (TestGetWordNumInString)
	{
		ATL::String testStr1 ("here, have some gum!");
		ATL::String testStr2 ("word");
		ATL::String emptyStr;

		CHECK (testStr1.getWordNumInString (1) == "here");
		CHECK (testStr1.getWordNumInString (2) == "have");
		CHECK (testStr1.getWordNumInString (4) == "gum");
		CHECK (testStr1.getWordNumInString (5) == "");

		CHECK (testStr1.getWordNumInString (-5) == "");
		CHECK (testStr1.getWordNumInString (-4) == "here");
		CHECK (testStr1.getWordNumInString (-1) == "gum");
		CHECK (testStr2.getWordNumInString (-1) == "word");

		CHECK (emptyStr.getWordNumInString (0) == "");
		CHECK (emptyStr.getWordNumInString (62) == "");
		CHECK (emptyStr.getWordNumInString (-62) == "");
	}
	
	TEST (TestGetNumWordsInString)
	{
		ATL::String testStr1 ("here, have some gum!");
		ATL::String testStr2 ("Fdslal asldeee   (() .al;al");
		ATL::String emptyStr;
		ATL::String whiteSpaceStr ("  	] ");
		
		CHECK (testStr1.getNumWordsInString() == 4);
		CHECK (testStr2.getNumWordsInString() == 4);
		CHECK (emptyStr.getNumWordsInString() == 0);
		CHECK (whiteSpaceStr.getNumWordsInString() == 0);
	}

	TEST (TestCStr)
	{
		const char testCStr[] = "An ordinary c string of the testing variety.";
		ATL::String testString (testCStr);

		CHECK (strlen (testString.cStr ()) == strlen (testCStr));
		CHECK (strcmp (testString.cStr (), testCStr) == 0);
	}
	
	TEST (TestSubStr)
	{
		ATL::String testString ("A mighty string!");
		
		CHECK (testString.subStr (0) == testString);
		CHECK (testString.subStr (7) == "y string!");
		CHECK (testString.subStr (15) == "!");
		CHECK (testString.subStr (2,6) == "mighty");
		
		// A start pos equal to string length should return an empty string.
		CHECK (testString.subStr (testString.length()).empty());
		
		// Out of range sub string should throw an error.
		CHECK_THROW (testString.subStr (17), std::runtime_error);
	}
	
	TEST (TestPadToNChars)
	{
		ATL::String testStr = "a test";
		const ATL::String origStr = "a test";
		ATL::String targetStr = "a test     ";
		
		testStr.padToNChars (3);
		CHECK (testStr == origStr);
		
		testStr.padToNChars (targetStr.length());
		CHECK (testStr == targetStr);
	}

	TEST (TestGetAsBool)
	{
		ATL::String testStr1 ("True");
		ATL::String testStr2 ("TRUE");
		ATL::String testStr3 ("true");
		ATL::String testStr4 ("false");
		ATL::String testStr5 ("1");
		ATL::String testStr6 ("0");
		ATL::String testStr7 ("tasdf33fg");

		CHECK (testStr1.getAsBool() == true);
		CHECK (testStr2.getAsBool() == true);
		CHECK (testStr3.getAsBool() == true);
		CHECK (testStr4.getAsBool() == false);
		CHECK (testStr5.getAsBool() == true);
		CHECK (testStr6.getAsBool() == false);

		CHECK_THROW (testStr7.getAsBool(), std::runtime_error);
	}

	TEST (TestGetAsDouble)
	{
		ATL::String testStr1 ("617.37");
		ATL::String testStr2 ("fl6;17.37");
		const double testInt = 617.37;

		CHECK (testStr1.getAsDouble() == testInt);

		//CHECK_THROW (testStr2.getAsDouble(), std::runtime_error);
	}

	TEST (TestDefaultAssignment)
	{
		ATL::String testString ("some more data");
		ATL::String newStringData ("different data");

		ATL::String* testStrRef;

		testStrRef = &(testString = newStringData);

		CHECK (testString.equalTo (newStringData));
		CHECK (&testString == testStrRef);
	}

	TEST (TestCStrAssignment)
	{
		ATL::String testString ("some more data");
		const char newCStrData[] = "different data";

		ATL::String* testStrRef;

		testStrRef = &(testString = newCStrData);

		CHECK (strcmp (testString.cStr(), newCStrData) == 0);
		CHECK (&testString == testStrRef);
	}

	TEST (TestDoubleAssignment)
	{
		ATL::String testString1 ("test1");
		ATL::String expectedResult ("617.37");
		const double testInt = 617.37;

		ATL::String* RetPtr;

		RetPtr = &(testString1 = testInt);

		CHECK (RetPtr == &testString1);
		CHECK (testString1.equalTo (expectedResult));
	}

	TEST (TestDefaultComparison)
	{
		ATL::String testString1 ("test1");
		ATL::String equalStr ("test1");
		ATL::String notEqualStr ("617.37");

		CHECK (testString1 == equalStr);
		CHECK (!(testString1 == notEqualStr));
	}

	TEST (TestCStringComparison)
	{
		const char equalCString[] = "test1";
		const char notEqualCString[] = "test2";
		ATL::String testString1 (equalCString);

		CHECK (testString1 == equalCString);
		CHECK (!(testString1 == notEqualCString));
	}
	
	TEST (TestDefaultUnequalComparison)
	{
		ATL::String testString1 ("test1");
		ATL::String equalStr ("test1");
		ATL::String notEqualStr ("617.37");

		CHECK (!(testString1 != equalStr));
		CHECK (testString1 != notEqualStr);
	}

	TEST (TestDefaultPlusOperator)
	{
		const ATL::String testString1 ("test1");
		const ATL::String addStr ("another");

		CHECK ((testString1 + addStr) == "test1another");
	}

	TEST (TestCStrPlusOperator)
	{
		const ATL::String testString1 ("test1");
		const char addStr[] = "another";

		CHECK ((testString1 + addStr) == "test1another");
	}

	TEST (TestDoublePlusOperator)
	{
		const ATL::String testString1 ("test1");
		const double addInt = 6432.5;

		CHECK ((testString1 + addInt) == "test16432.5");
	}

	TEST (TestDefaultAppendOperator)
	{
		ATL::String testString1 ("test1");
		ATL::String testString2 ("world");

		ATL::String* RetPtr;

		RetPtr = &(testString1 += testString2);

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1world"));
	}

	TEST (TestAppendOperatorDouble)
	{
		ATL::String testString1 ("test1");
		ATL::String expectedResult ("test1617.37");
		const double testInt = 617.37;

		ATL::String* RetPtr;

		RetPtr = &(testString1 += testInt);

		CHECK (RetPtr == &testString1);
		CHECK (testString1.equalTo (expectedResult));
	}

	TEST (TestSubScriptOpConst)
	{
		StringMockObject str ("01234567");

		CHECK (str.charAt (0) == '0');
		CHECK (str.charAt (4) == '4');
		CHECK (str.charAt (7) == '7'); // Test last array item

		// Check that an exception is thrown for out-of-bounds indexes.
		CHECK_THROW (str.charAt (8), std::runtime_error);
	}

	
	TEST (TestSubScriptOpAssing)
	{
		StringMockObject str ("01234567");
		StringMockObject expectedResult ("0123!567");

		CHECK (str [4] == '4');

		str [4] = '!';

		CHECK (str == expectedResult);
	}
	
	TEST (TestInsertionOpString)
	{
		//String& operator<< (const String& val);
		ATL::String testString1 ("someText");
		ATL::String testString2 ("Moar Text!");
		ATL::String expectedResult ("someTextMoar Text!");
		
		ATL::String* RetPtr;
		
		RetPtr = &(testString1 << testString2);
		
		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1 == expectedResult);
	}
	
	TEST (TestInsertionOpCStr)
	{
		ATL::String testString1 ("test1");
		const char appStr[] = "hello";

		ATL::String* RetPtr;

		RetPtr = &(testString1 << appStr);

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1hello"));
	}
	
	TEST (TestInsertionOpChar)
	{
		ATL::String testString1 ("test1");
		const char testChar = 'C';

		ATL::String* RetPtr;

		RetPtr = &(testString1 << testChar);

		// Test for the return reference.
		CHECK (RetPtr == &testString1);

		CHECK (testString1.equalTo ("test1C"));
	}

	TEST (TestInsertionOpDouble)
	{
		ATL::String testString1 ("test1");
		ATL::String expectedResult ("test1617.37");
		const double testInt = 617.37;

		ATL::String* RetPtr;

		RetPtr = &(testString1 << testInt);

		CHECK (RetPtr == &testString1);
		CHECK (testString1.equalTo (expectedResult));
	}
	
	TEST (TestLessThanOperator)
	{
		ATL::String testString1 ("ccc");
		ATL::String testString2 ("ccb");
		ATL::String testString3 ("ccc");
		ATL::String testString4 ("cccc");

		CHECK ((testString1 < testString2) == false);
		CHECK ((testString2 < testString1) == true);
		CHECK ((testString1 < testString3) == false);
		CHECK ((testString3 < testString1) == false);
		
		// More characters means greather than a string with fewer chars.
		CHECK ((testString1 < testString4) == true);
	}

	TEST (TestReadLineFromConsole)
	{
		StringMockObject str;
		std::stringstream mockIOStream;

		const char testStr[] = "some test Data";
		StringMockObject expectedResult (testStr);

		// Setup mock objects for the cin/cout handles.
		str.setInStream (&mockIOStream);

		mockIOStream << testStr;

		// Check the number of bytes returned.
		CHECK (str.readLineFromConsole (1000) == strlen (testStr));

		CHECK (str == expectedResult);
	}

	TEST (TestPrintLineToConsole)
	{
		std::stringstream mockIOStream;

		const char testCStr[] = "some test Data";
		StringMockObject testString (testCStr);

		// Setup mock objects for the cin/cout handles.
		testString.setOutStream (&mockIOStream);

		CHECK (testString.writeToConsole() == strlen (testCStr));

		CHECK (testString == mockIOStream.str().c_str());
	}
}


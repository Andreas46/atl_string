#pragma once

#include <unittest++/TestReporter.h>
#include <unittest++/UnitTest++.h>

#include <ostream>
#include <string>

//class UNITTEST_LINKAGE FileTestReporter: public UnitTest::TestReporter
class FileTestReporter: public UnitTest::TestReporter
{
public:
	FileTestReporter(std::ostream& targetOutput);
	~FileTestReporter(void);

	int runAllUnitTests (void);

private:
	bool hasTestFailed;

	std::ostream* output;

	virtual void ReportTestStart (UnitTest::TestDetails const& test);
    virtual void ReportFailure (UnitTest::TestDetails const& test, char const* failure);
    virtual void ReportTestFinish (UnitTest::TestDetails const& test, float secondsElapsed);
    virtual void ReportSummary (int totalTestCount, int failedTestCount, int failureCount, float secondsElapsed);

	std::string getTimeStamp (void);
};


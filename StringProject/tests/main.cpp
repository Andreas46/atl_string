#include <iostream>

#include <unittest++/UnitTest++.h>
//#include <UnitTest++/XmlTestReporter.h>
//#include <UnitTest++/TestReporterStdout.h>

#include "FileTestReporter.h"

#include <fstream>
#include <sstream>

// The string library it's self is a library and does not require a main file.
// This file is purely to invoke the unit tests.
int main (void)
{
	int retCode;

	std::stringstream outStr; // Temporary output string stream.
	std::ofstream testLogFile;

	FileTestReporter reporter (outStr);

	retCode = reporter.runAllUnitTests();

	testLogFile.open ("./tests.log");
	testLogFile << outStr.str();
	testLogFile.close();

	std::cout << "blash" << std::endl;

	std::cout << outStr.str() << std::endl;

	#ifdef _WIN32
		// Wait for user input before exiting.
		std::cin.clear();
		std::cin.get();
	#endif

	return retCode;
}


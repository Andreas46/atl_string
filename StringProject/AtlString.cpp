#include "AtlString.h"

#include <cctype>
#include <cstdlib>
#include <cstring>

#include <sstream>
#include <stdexcept>
#include <vector>


ATL::String::String(void)
{
	initialiseClassState ();
}

ATL::String::String (const String& rhs)
{
	initialiseClassState ();

	setData (rhs.data, rhs.dataLen);
}

ATL::String::String (const char* cStr)
{
	initialiseClassState ();

	size_t initialSize = strlen (cStr);

	setData (cStr, initialSize);
}
	
ATL::String::String (const char* byteArry, const uint32_t numBytes)
{
	initialiseClassState ();
	setData (byteArry, numBytes);
}

ATL::String::String (const int inInt)
{
	initialiseClassState ();
	
	append (static_cast <double> (inInt));
}

ATL::String::String (const double inInt)
{
	initialiseClassState ();

	//// The maximum number of digits which can be represented by a double.
	//// Warning: this is in the order of 1000 (on a 64bit system).  So 1kb of memory 
	//// Is to allocated every time this function is called.
	//// Ref: http://stackoverflow.com/questions/1701055/what-is-the-maximum-length-in-chars-needed-to-represent-any-double-value
	////const size_t max_digits = 3 + DBL_MANT_DIG - DBL_MIN_EXP;

	////int64_t wholeNum;
	////double remainder;

	////char buff [max_digits + 1]; // +1 For null terminator. We'll be allocating a c string initially.
	////int numCharsWritten;

	////wholeNum = (int64_t) inInt;
	////remainder = inInt - wholeNum;

	////void* remainderHack = &remainder;

	//// Mask out the sign, and exponent bits from the double.  This will result
	//// in a positive integer representation of the remainder.
	////remainder = ((*((int64_t*) remainderHack)) & 0xFFFFFFFFFFFF);
	//
	////std::sprintf (buff, "%lld.%llx", wholeNum, remainder);

	////numCharsWritten = std::sprintf (buff, "%d", wholeNum);  // Unsafe due to printf implementation bugs?
	////std::sprintf (buff + numCharsWritten, "%f", remainder);

	//// Set initial data.

	//std::stringstream strStream;
	//std::string str;

	////double newInt = -991501;

	//// Set strStream precision to MAXIMUM!!
	//// 17 significant decimal digits being the maximum precision that a 64bit double can represent.
	////strStream.precision (17);

	//// Always used fixed decimal format (no exponential).
	////strStream.setf (std::stringstream::fixed);

	//strStream << inInt;
	//str = strStream.str();

	//setData (str.c_str(), str.length());
	////std::cout << str << "." << std::endl;
	////std::cout << str.length() << length() << std::endl;

	append (inInt);
}

WIN_EX ATL::String::String (const bool inVal)
{
	initialiseClassState ();
	
	if (inVal == true)
	{
		append ("true");
	}
	else // False.
	{
		append ("false");
	}
}

ATL::String::String (const char c)
{
	initialiseClassState ();
	setData (&c, 1);
}

ATL::String::~String (void)
{
	if (data != NULL)
	{
		std::free (data);
	}

	if (cStrPtr != NULL)
	{
		std::free (cStrPtr);
	}
}

size_t ATL::String::length (void) const
{
	return dataLen;
}

void ATL::String::clear (void)
{
	resizeCharArray (0);
	dataLen = 0;
}

bool ATL::String::empty (void) const
{
	if (dataLen == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

const char& ATL::String::charAt (size_t index) const
{
	if (index >= length())
	{
		throw std::runtime_error ("ATL::STRING: Bad array index.");
	}

	return data [index];
}

char& ATL::String::charAt (size_t index)
{
	if (index >= length())
	{
		throw std::runtime_error ("ATL::STRING: Bad array index.");
	}

	return data [index];
}

bool ATL::String::equalTo (const String& rhs) const
{
	// Are the strings of the same length, to begin with.
	if (length() != rhs.length())
	{
		return false;
	}

	// Compare each element of the strings for discrepancies.
	for (size_t i = 0; i < length(); ++i)
	{
		if (data [i] != rhs.data [i])
		{
			// This character differs in the strings.  They're not equal.
			return false;
		}
	}

	return true;
}

ATL::String& ATL::String::append (const String& rhs)
{
	return append (rhs.data, rhs.dataLen);
}

ATL::String& ATL::String::append (const char* appData, const size_t appDataLen)
{
	const size_t newStrLen = dataLen + appDataLen;

	resizeCharArray (newStrLen);  // Make it bigger.

	// Copy data from "appData" to end of string.
	// + dataLen moves to end of existing data.
	memmove (data + dataLen, appData, appDataLen); // Remember: Dest, src, size.

	// New string length;
	dataLen = newStrLen;

	return *this;
}

ATL::String& ATL::String::append (const char& c)
{
	const size_t newStrLen = dataLen + 1;

	resizeCharArray (newStrLen);

	data [dataLen] = c;

	// New string length;
	dataLen = newStrLen;

	return *this;
}

ATL::String& ATL::String::append (const double appInt)
{
	std::stringstream strStream;
	std::string str;

	strStream << appInt;
	str = strStream.str();

	append (str.c_str(), str.length());

	return *this;
}

ATL::String& ATL::String::prepend (const String& rhs)
{
	// Create a new string with the same contents as this string.
	String strEnd (*this);

	// Reset this string.
	clear();

	// Set the contents of this string to that of rhs.
	append (rhs);

	// Then add what was originally in this string to the end.
	append (strEnd);

	return *this;
}

void ATL::String::toUpper (void)
{
	for (size_t i = 0; i < dataLen; ++i)
	{
		data [i] = std::toupper (data [i]);
	}
}

void ATL::String::toLower (void)
{
	for (size_t i = 0; i < dataLen; ++i)
	{
		data [i] = std::tolower (data [i]);
	}
}

int64_t ATL::String::find (const String& str, const size_t pos) const
{
	const int64_t retErrorCode = -1;

	// Always return retErrorCode if str is empty.
	if (str.dataLen == 0)
	{
		return retErrorCode;
	}
	
	// Always return retErrorCode if this string is empty.
	if (dataLen == 0)
	{
		return retErrorCode;
	}

	// while i is less than dataLen - (length of data - 1).  For loop is
	// written the way it is to account for use of unsigned variables.
	for (size_t i = pos; i + str.dataLen < dataLen + 1; ++i)
	{
		int64_t memCmpRetCode = std::memcmp (data + i, str.data, str.dataLen);

		if (memCmpRetCode == 0)
		{
			// Found occurrence!
			return i;
		}
	}

	return retErrorCode;
}

//#include <string>

int64_t ATL::String::replace (const String& findStr,
							  const String& replaceStr,
							  const bool replaceAll,
							  const size_t pos,
							  const bool replaceWholeWord)
{
	String originalStr (data, dataLen);  // Copy of this string.
	int64_t numRepalcements = 0;
	size_t origStrPos = 0;
	int64_t findPos = 0;

	// Reset string.
	clear();

	if (replaceWholeWord)
	{
		findPos = originalStr.findWordPosInString (findStr, pos);
	}
	else
	{
		findPos = originalStr.find (findStr, pos);
	}

	// Note: findPos must be a signed int to check for "-1" events.  It's safe to cast
	// it to size_t within this loop, since it's sign has been checked in the while condition.
	while (findPos >= 0)
	{
		// Append the portion of the original string up until the find pos.
		append (originalStr.data + origStrPos, (size_t) (findPos - origStrPos));

		// Append the replacement string.
		append (replaceStr);

		++numRepalcements;

		// Advance the original string position by the length of the replaced
		// string beyond where the replacement was found.
		origStrPos = (size_t) findPos + findStr.dataLen;

		// If we're only making the first replacement, we're done.
		if (replaceAll == false)
		{
			break;
		}

		// Look for next replacement
		if (replaceWholeWord)
		{
			findPos = originalStr.findWordPosInString (findStr, origStrPos);
		}
		else
		{
			findPos = originalStr.find (findStr, origStrPos);
		}
	}

	// Append remainder of original string.
	append (originalStr.data + origStrPos, originalStr.dataLen - origStrPos);

	return numRepalcements;
}

ATL::String ATL::String::getNextWord (size_t startPos) const
{
	ATL::String retString;

	if (startPos > dataLen)
	{
		throw std::runtime_error ("getNextWord: Invalid startPos!");
	}

	// while i is less than dataLen - (length of data - 1).  For loop is
	// written this way it is to account for use of unsigned variables.
	for (size_t i = startPos; i < dataLen; ++i)
	{
		if (isspace (data [i]) ||
			// Or is punctuation which is not a '-' or '_'.
			(ispunct (data [i]) && data [i] != '-' && data [i] != '_'))
		{
			// Skip leading white space.
			if (retString.empty())
			{
				continue;
			}
			break;
		}
		else
		{
			retString += data [i];
		}
	}

	return retString;
}

int64_t ATL::String::findWordPosInString (const ATL::String& wordToFind,
										  const size_t startPos) const
{
	int64_t findPos;

	findPos = find (wordToFind, startPos);

	if (findPos >= 0)
	{
		// We found the string, but was it a full word?

		// Test for word bounds at start.
		if (findPos != 0 && // Word at beginning of string.
			(isalnum (charAt ((size_t)findPos - 1)) ||
				charAt ((size_t)findPos - 1) == '-' ||
				charAt ((size_t)findPos - 1) == '_'))
		{
			// We found the string, but not as a discrete word.
			findPos = -1;
		}

// 		// Test for word bounds at end of word position.
// 		else if ((size_t)findPos + wordToFind.length() != length() && // word at end?
// 			charAt((size_t) findPos + wordToFind.length()) != ' ') // word followed by space?
		// Test for word bounds at end of word position.
		else if ((size_t)findPos + wordToFind.length() != length())
		{
			char endChar = charAt((size_t) findPos + wordToFind.length());

			if (isalnum (endChar) || endChar == '-' || endChar == '_')
			{
				// We found the string, but not as a discrete word.
				findPos = -1;
			}
		}
	}

	return findPos;
}

ATL::String ATL::String::getWordNumInString (const int64_t wordNum) const
{
	std::vector <ATL::String> wordList;

	//std::cerr << "about to make list" << std::endl;

	wordList.push_back ("");

	// Generate a vector off all words in string.
	for (size_t i = 0; i < dataLen; ++i)
	{
		if (isalnum (data [i]) || data [i] == '_' || data [i] == '-')
		{
			wordList.back().append (data[i]);
		}
		else if (!wordList.back().empty())
		{
			wordList.push_back ("");
		}
	}

	// Strip the last line if it's empty.
	if (wordList.back().empty())
	{
		wordList.pop_back();
	}

	// Handle the positive "wordNum"
	if (wordNum > 0 && (size_t) wordNum <= wordList.size())
	{
		return wordList [(size_t) wordNum -1];
	}
	else if (wordNum < 0 && (int64_t) wordList.size() + wordNum >= 0)
	{
// 		std::cerr << "msdlist" << std::endl;
		return wordList [wordList.size() + (size_t) wordNum];
	}
	else
	{
		return "";
	}
}

size_t ATL::String::getNumWordsInString (void) const
{
	size_t numWords = 0;
	bool onWord = false;

	// Generate a vector off all words in string.
	for (size_t i = 0; i < dataLen; ++i)
	{
		if (isalnum (data [i]))
		{
			// Are we starting a new word?
			if (onWord == false)
			{
				++numWords;
				onWord = true;
			}
		}
		else if (onWord == true)
		{
			// Finished a word.
			onWord = false;
		}
	}
	
	return numWords;
}

const char* ATL::String::cStr (void)
{
	// Make sure we have the correct amount of memory for the cString.
	cStrPtr = reinterpret_cast <char*> (std::realloc (cStrPtr, dataLen + 1)); // +1 for Null.

	// Copy over data.
	memmove (cStrPtr, data, dataLen); // Remember: Dest, src, size.

	// Set null terminator.
	cStrPtr [dataLen] = '\0';

	return cStrPtr;
}

ATL::String ATL::String::subStr (const size_t startPos, const int64_t len) const
{
	String newString;

	size_t maxPos;

	if (len <= -1)
	{
		// Set maxPos to maximum value.
		maxPos = ((size_t) 0) - (size_t) 1;
	}
	else
	{
		maxPos = (size_t) len;
	}

	if (startPos > dataLen)
	{
		// Exception.
		throw std::runtime_error ("Bad string startpos");
	}

	for (size_t i = startPos; i < dataLen && i - startPos < maxPos; ++i)
	{
		newString.append (data [i]);
	}

	return newString;
}

void ATL::String::padToNChars (const size_t padToSize, const char padChar)
{
	while (dataLen < padToSize)
	{
		append (padChar);
	}
}

size_t ATL::String::readLineFromConsole (const size_t maxBytes)
{
	size_t i;

	for (i = 0; i < maxBytes; ++i)
	{
		char nextChar;
		nextChar = (char) inStream->get();

		if (inStream->fail())
		{
			// Failed to read the last byte.
			break;
		}

		append (nextChar);
	}

	return i;
}

size_t ATL::String::writeToConsole (void)
{
	(*outStream) << cStr();

	return dataLen;
}

bool ATL::String::getAsBool (void) const
{
	ATL::String normalisedStr = *this;

	normalisedStr.toLower();

	if (normalisedStr == "true" ||
		normalisedStr == "1")
	{
		return true;
	}
	else if (normalisedStr == "false" ||
		normalisedStr == "0")
	{
		return false;
	}
	else
	{
		// This value can not be converted to a bool.
		ATL::String errMsg;
		errMsg << "Can not convert string '" << *this << "' to bool!.";
		throw std::runtime_error (errMsg.cStr());
	}
}

double ATL::String::getAsDouble (void) const
{
	//const size_t max_digits = 3 + DBL_MANT_DIG - DBL_MIN_EXP;
	//char tempStr [max_digits + 1]; // +1 for null terminator.
	char tempStr [1024]; // +1 for null terminator.

	memcpy (tempStr, data, dataLen); //Dest, src, numBytes.
	tempStr [dataLen] = '\0'; // Null terminator.

	return std::atof (tempStr);
}

uint64_t ATL::String::getAsUint64 (void) const
{
	return 0;
}

int64_t ATL::String::getAsInt64 (void) const
{
	return 0;
}

ATL::String& ATL::String::operator= (const String& rhs)
{
	clear();

	setData (rhs.data, rhs.dataLen);

	return *this;
}

ATL::String& ATL::String::operator= (const char* rhs)
{
	clear();

	setData (rhs, strlen (rhs));

	return *this;
}

ATL::String& ATL::String::operator= (const double& rhs)
{
	clear();

	append (rhs);

	return *this;
}

bool ATL::String::operator== (const String& rhs) const
{
	return equalTo (rhs);
}

bool ATL::String::operator!= (const String& rhs) const
{
	return !equalTo (rhs);
}

ATL::String ATL::String::operator+ (const String str) const
{
	ATL::String newStr (*this);

	newStr.append (str);

	return newStr;
}

 ATL::String& ATL::String::operator+= (const String rhs)
{
	append (rhs);
	return *this;
}

const char& ATL::String::operator[] (const unsigned int index) const
{
	return charAt (index);
}

char& ATL::String::operator[] (const unsigned int index)
{
	return charAt (index);
}

ATL::String& ATL::String::operator<< (const String& val)
{
	append (val);
	return *this;
}

bool ATL::String::operator< (const ATL::String& rhs) const
{
	// Compare lengths first.
	if (dataLen < rhs.dataLen)
	{
		return true;
	}
	else if (dataLen > rhs.dataLen)
	{
		return false;
	}
	
	// Length is equal.  Perform binary comparison.
	if (memcmp (data, rhs.data, dataLen) < 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ATL::String::initialiseClassState (void)
{
	data = NULL;
	cStrPtr = NULL;
	dataLen = 0;
	inStream = &std::cin;
	outStream = &std::cout;
}

void ATL::String::resizeCharArray (size_t newSize)
{
	data = reinterpret_cast <char*> (std::realloc (data, newSize));
	
	// Check that data was properly allocated.
	if (newSize != 0 && data == NULL)
	{
		throw std::bad_alloc();
	}

	// Check for "free" condition.
	if (newSize == 0)
	{
		data = NULL;
	}
}

void ATL::String::setData (const char* src, size_t newDataLen)
{
	dataLen = newDataLen;

	resizeCharArray (newDataLen);

	memmove (data, src, newDataLen); // Remember: Dest, src, size.
}

#pragma once

#include <stdint.h>

#include <iostream>

// For winblows. |:-[
#ifdef _WIN32
	#define WIN_EX __declspec (dllexport)
#else
	#define WIN_EX
#endif

namespace ATL
{
	class String
	{
	public:
		/**
		* @brief default constructor.
		**/
		WIN_EX String (void);

		/**
		* @brief Default initialiser constructor.
		*
		* @param rhs
		*	Initialiser.
		**/
		WIN_EX String (const String& rhs);

		/**
		* @brief C string constructor.
		*
		* Construct a string from an existing c-style character array.
		*
		* @param cStr  Initialiser.
		**/
		WIN_EX String (const char* cStr);

		/**
		* @brief Byte array initialiser.
		* 
		* Initialise a new String with numBytes worth of data from array cStr.
		* 
		* @param byteArry Array to take data from
		* 
		* @return the new instance of String.
		**/
		WIN_EX String (const char* byteArry, const uint32_t numBytes);

		/**
		* @brief Character constructor.
		*
		* Construct a string representation of a single character.
		*
		* @param inInt
		* 	Initialiser.
		**/
		WIN_EX String (const char c);

		/**
		* @brief C string constructor.
		*
		* Construct a string representation of an integer.
		*
		* @param inInt  Initialiser.
		**/
		WIN_EX String (const int inInt);

		/**
		* @brief Double constructor.
		*
		* Construct a string representation of a floating point integer.
		*
		* @param inInt  Initialiser.
		**/
		WIN_EX String (const double inInt);

		/**
		* @brief Constructor that takes a bool.
		*
		* A string representation of the bool (All lowercase)
		*
		* @param inVal  Initialiser.
		**/
		WIN_EX String (const bool inVal);

		/**
		* @brief default destructor.
		**/
		WIN_EX ~String(void);

		/**
		* @brief Return the length of the string.
		*
		* @return Unsigned integer representing the size of the string.
		**/
		WIN_EX size_t length (void) const;

		/**
		* @brief Reset this string to it's default state.
		**/
		WIN_EX void clear (void);

		/**
		* @brief Return true if this string has no data in it.
		**/
		WIN_EX bool empty (void) const;
		
		/**
		* @brief Get the character at string position "index".
		*
		*
		* @param index
		*	The position in the string at which to return the character.
		*
		* @except std::runtime_error
		*	Thrown when an invalid index is provided.
		*
		* @return
		**/
		WIN_EX const char& charAt (size_t index) const;

		/**
		* @brief Return modifiable char reference in data array.
		*
		* @param index
		*	The position in the string at which to return the character.
		*
		* @except std::runtime_error
		*	Thrown when an invalid index is provided.
		*
		* @return
		**/
		WIN_EX char& charAt (size_t index);

		/**
		* @brief Compare strings.
		*
		* @param rhs
		*	The string to compare to
		*
		* @return True if rhs is equal to this string, or false otherwise.
		**/
		WIN_EX bool equalTo (const String& rhs) const;

		/**
		* @brief Append the contents of "rhs" to this string object.
		*
		* @param rhs
		*	The string to append.
		*
		* @return A reference to this string.
		**/
		WIN_EX String& append (const String& rhs);

		/**
		* @brief Append a character array to the end of this string.
		*
		* @param data
		*	The data to append.
		*
		* @param dataLen
		*	The length of the data to append.
		*
		* @return A reference to this string.
		**/
		WIN_EX String& append (const char* appData, const size_t appDataLen);

		/**
		* @brief Append a single character.
		*
		* @param c
		*	The data to append.
		*
		* @return A reference to this string.
		**/
		WIN_EX String& append (const char& c);

		/**
		* @brief Append the ascii representation of a double to this string
		*
		* @param appInt
		*	The integer to append.
		*
		* @return A reference to this string.
		**/
		WIN_EX String& append (const double appInt);

		/**
		* @brief Prepend the contents of "rhs" to the beginning of this string.
		*
		* @param rhs
		*	The string to prepend.
		*
		* @return A reference to this string.
		**/
		WIN_EX String& prepend (const String& rhs);

		/**
		* @brief Convert all characters to their upper-case equivalent.
		**/
		WIN_EX void toUpper (void);

		/**
		* @brief Convert all characters to their lower-case equivalent.
		**/
		WIN_EX void toLower (void);

		/**
		* @brief Find the first occurrence of string "str" within this object.
		*
		*
		* @param str
		*	The string to search for within this object.
		*
		* @parm pos
		*	The index of the first character to be considered in the search.
		*
		* @return The index of the fist letter of the first occurrence of
		*	str if found, or -1 if not found.
		**/
		WIN_EX int64_t find (const String& str, const size_t pos = 0) const;

		/**
		* @brief Replace instances of "str" within this object with "replaceStr".
		*
		* @param findStr
		*	The string to search for within this object
		*
		* @param repalceStr
		*	The string to replace occurrences of "str" with.
		*
		* @param replaceAll
		*	If set to false, replaces first occurrence of "str".  If set to true
		*	(default) all instances of "str" will be replaced with this object.
		*
		* @parm pos
		*	The index of the first character to be considered in the search.
		*
		* @return The number of replacements performed.
		**/
		WIN_EX int64_t replace (const String& findStr,
								const String& replaceStr,
								const bool replaceAll = true,
								const size_t pos = 0,
								const bool replaceWholeWord = false);

		/**
		* @brief Get the next full word in this string, treating "startPos" as
		* 	the first letter of that word.
		*
		* @param startPos
		* 	Where in the string to start searching.
		*
		* @return
		* 	The "word" as delimited by white space, or the end of string.
		**/
		WIN_EX String getNextWord (size_t startPos = 0) const;

		/**
		* @brief Return the position of the first occurance of "wordToFind".
		*
		* @param wordToFind
		* 	The word to search for in this string.
		*
		* @param startPos
		* 	Where to begin the search from.
		**/
		WIN_EX int64_t findWordPosInString (const ATL::String& wordToFind,
											const size_t startPos = 0) const;

		/**
		* @brief Get the Nth word in the string.
		*
		* @param wordNum
		* 	The Nth word to get.  If this value is negative, get the Nth
		* 	word from the end of the string.
		*
		* @return the Nth word, or an empty string for invalid "wordNum".
		**/
		WIN_EX ATL::String getWordNumInString (const int64_t wordNum) const;

		/**
		* @brief Return the number of words contained in this string.
		* 
		* @return Number of words in the string.
		**/
		WIN_EX size_t getNumWordsInString (void) const;

		/**
		* @brief Return a const reference to a c-style string.
		*
		* This function returns a const reference to the internal data
		* structure, and so the pointer will remain valid for the life-
		* time of this object, but so too, will it be modified as this
		* object is modified.
		*
		* @return C-style string representation of this object.
		**/
		WIN_EX const char* cStr (void);

		/**
		* @brief Get a new string comprised of a section of this string.
		*
		* @param startPos
		*	Where to start getting the sub string.
		*
		* @param len
		*	How many character to get.
		*
		* @return
		**/
		WIN_EX String subStr (const size_t startPos, const int64_t len = -1) const;

		/**
		* @brief Expand the string by appending "padChar" to the end.
		* 
		* If the string length is less than "padToSize", no changes occurs.
		* Otherwise the character "padChar" is appended until the string reaches
		* a length of "padToSize".
		* 
		* @param padToSize
		* 	The size to pad the string to.
		* 
		* @param padChar
		* 	The character to use as padding.  Space by default.
		**/
		WIN_EX void padToNChars (const size_t padToSize, const char padChar = ' ');

		/**
		* @brief Read a line from std::cin into this string.
		*
		* This function is abhorrent abandon of common Containerisation practices.
		* It should not be used.  If you use it, you will give me nightmares. ]:-[
		*
		* @param maxBytes
		*	Maximum number of bytes to consider.
		*
		* @return The number of bytes read from cin.
		**/
		WIN_EX size_t readLineFromConsole (const size_t maxBytes);

		/**
		* @brief Print the contents of this string to cout.
		*
		* I feel so dirty.
		*
		* @return The number of bytes written to cout;
		**/
		WIN_EX size_t writeToConsole (void);

		// --- Interpreting strings as other types --- //

		/**
		* @brief Interpret this string as a boolean value.
		**/
		WIN_EX bool getAsBool (void) const;

		/**
		* @brief Interpret this string as a double precicion floating point number.
		**/
		WIN_EX double getAsDouble (void) const;

		/**
		* @brief Interpret this string as an unsigned integer.
		**/
		WIN_EX uint64_t getAsUint64 (void) const;

		/**
		* @brief Interpret this string as an signed integer.
		**/
		WIN_EX int64_t getAsInt64 (void) const;

		// --- Assignment --- //

		/**
		* @brief Default assignment
		*
		* @return This string.
		**/
		WIN_EX String& operator= (const String& rhs);

		/**
		* @brief C string assignment
		* 
		* Assigning a cString to this class.
		* 
		* @param rhs char array to initialise initial data from.
		*
		* @return This string.
		**/
		WIN_EX String& operator= (const char* rhs);

		/**
		* @brief Floating point integer assignment
		* 
		* Assign a string representation of a floating point integer to this class.
		* 
		* @param rhs char array to initialise initial data from.
		*
		* @return This string.
		**/
		WIN_EX String& operator= (const double& rhs);

		// --- Comparison Operators --- //

		/**
		* @brief default comparison operator.
		**/
		WIN_EX bool operator== (const String& rhs) const;

		/**
		* @brief default unequal comparison operator.
		**/
		WIN_EX bool operator!= (const String& rhs) const;

		// --- Plus Operators --- //

		/**
		* @brief Create a new String with "rhs" appended to it.
		*
		* @param rhs
		*	The content to append to the end of the new string.
		*
		* @return A new instance of the String with this, and "rhs" appended to the end.
		**/
		WIN_EX String operator+ (const String str) const;

		// --- Append Operators --- //

		/**
		* @brief Append "rhs" to this instance of String.
		*
		* @param rhs
		*	The content to append to the end of this string
		*
		* @return a reference to this string.
		**/
		WIN_EX String& operator+= (const String rhs);

		/**
		* @brief SubScriptOperator
		*
		* Return the character that is at position "index".
		*
		* @return reference to "index", the provided parameter.
		**/
		WIN_EX const char& operator[] (const unsigned int index) const;

		/**
		* @brief SubScriptOperator
		*
		* Return a modifiable reference to the character that is at position "index".
		*
		* @return reference to "index", the provided parameter.
		**/
		WIN_EX char& operator[] (const unsigned int index);
		
		// --- Insertion operators. --- //
		
		/**
		* @brief Append "val" to this string;
		* 
		* @param val
		* 	The value to append to the string;
		* 
		* @return A reference to this object.
		**/
		WIN_EX String& operator<< (const String& val);
		
		/**
		* @brief Append "val" to this string;
		* 
		* @param val
		* 	The value to append to the string;
		* 
		* @return A reference to this object.
		**/
		//WIN_EX String& operator<< (const bool val);
		
		/**
		* @brief Less than comparison operator.
		* 
		* This is useful for using this class as a key in std::map, etc.
		* 
		* @param rhs
		* 	The string to compare to.
		* 
		* @return A reference to this object.
		**/
		WIN_EX bool operator< (const ATL::String& rhs) const;

	protected:
		char* data;
		char* cStrPtr;
		size_t dataLen;

		std::istream* inStream;
		std::ostream* outStream;

		void initialiseClassState (void);

		void resizeCharArray (size_t newSize);

		void setData (const char* src, size_t dataLen);
	};
}
